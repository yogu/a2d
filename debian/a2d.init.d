#!/bin/bash
. /lib/lsb/init-functions

### BEGIN INIT INFO
# Provides:          a2d
# Required-Start:    $remote_fs $syslog network-online.target
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: a2d HTML app
### END INIT INFO

# Change these values to match your application
APP_DIR="/usr/lib/python3/dist-packages/a2d"
APP_USER="a2d"
GUNICORN_CONFIG="/usr/lib/python3/dist-packages/a2d/gunicorn_config.py"

case "$1" in
  start)
    echo "Starting a2d HTML App..."
    cd $APP_DIR
    sudo -u $APP_USER /usr/bin/python3 -m gunicorn app:app -c $GUNICORN_CONFIG &
    ;;
  stop)
    echo "Stopping a2d HTML App..."
    pkill -f "gunicorn: master"
    ;;
  restart)
    $0 stop
    sleep 1
    $0 start
    ;;
  force-reload)
    echo "Reloading a2d HTML App..."
    $0 stop
    sleep 1
    $0 start
    ;;
  status)
    status_of_proc -p /var/run/a2d.pid "" "a2d HTML App"
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|force-reload|status}"
    exit 1
    ;;
esac

exit 0
